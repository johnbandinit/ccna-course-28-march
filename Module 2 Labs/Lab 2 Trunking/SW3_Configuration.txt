#####Switch 3 Configuration#####
!
en
!
conf t
!
hostname SW3
!
vlan 10 
 name SALES
 exit
!
vlan 20
 name ACCOUNTING
 exit
!
vlan 30
 name MGMT
 exit
!
vlan 40
 name VOICE
 exit
!
int gi0/1
 description ///ACCOUNTING DEPT\\\
 switchport mode access
 switchport access vlan 20
 exit
!
int gi0/0
 description ///SALES DEPT\\\
 switchport mode access
 switchport access vlan 10
 switchport voice vlan 40
 exit
!
int gi1/0 
 description ///TRUNK TO SW2\\\
 description ///TRUNK TO SW1\\\
 switchport trunk encapsulation dot1q
 switchport mode trunk
 switchport nonegotiate
 switchport trunk allowed vlan 1,10,20,30,40
 exit
!
int gi2/0
 description ///TRUNK TO SW1\\\
 switchport trunk encapsulation dot1q
 switchport mode trunk
 switchport nonegotiate
 switchport trunk allowed vlan 1,10,20,30,40
 exit
!
int vlan 1
 shutdown
 exit
!
int vlan 30
 description ///MGMT SVI\\\
 ip address 22.3.4.195 255.255.255.240
 no shut
 exit
!
ip default-gateway 22.3.4.193 
!
do wr